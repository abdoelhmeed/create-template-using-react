import "./App.css";
import Navbar from "./Components/Navbar/Navbar";
import Index from "./Components/Index/Index";
import Footer from "./Components/Footer/Footer";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Contact from "./Components/Contact/Contact";

function App() {
  return (
    <BrowserRouter>
    <div>
      <Navbar />
      <Routes>
        <Route extends path="/" element={<Index />} />
        <Route path="/Contact" element={<Contact />} />
      </Routes>
      <Footer />
      </div>
    </BrowserRouter>
  );
}

export default App;
