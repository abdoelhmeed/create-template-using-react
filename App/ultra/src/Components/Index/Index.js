import Home  from "./../Home/Home";
import About  from "./../About/About";
import SocialMedia  from "./../SocialMedia/SocialMedia";
import Portfolio  from "./../Portfolio/Portfolio";
import Work  from "./../Work/Work"


function Index() {
  return (
    <div>
      <Home/>
      <Work/>
      <About />
      <Portfolio />
      <SocialMedia/>
    </div>
  );
}


export default Index;
